extern crate find_folder;
#[macro_use]
extern crate conrod;
extern crate rumqtt;
extern crate serde_json;
extern crate toml;
#[macro_use]
extern crate serde_derive;
extern crate rand;
use conrod::backend::glium::glium;
use conrod::backend::glium::glium::Surface;
use conrod::Borderable;
use rumqtt::{MqttOptions, MqttClient, MqttCallback, QoS};
use std::io::Read;
use std::fs::File;
use std::sync::mpsc::{Receiver, Sender, channel};
use std::sync::{Mutex, Arc};
use rand::prelude::*;



#[derive(Deserialize, Clone)]
struct BrokerConfig {
    address: String,
    user: String,
    password: String,
}

#[derive(Deserialize)]
struct Info {
    teams: Vec<String>,
    problems: u32,
}

// Generate a unique `WidgetId` for each widget.
widget_ids! {
    struct Ids {
        master,
        header,
        body,
        title,
        send_button,
        send_box,
        team_selector,
        team_box,
        team_title,
        team_title_box,
        titled_team_box,
        problem,
        problem_box,
        problem_title,
        problem_title_box,
        titled_problem_box,
        team_problem_row,
        value,
        value_box,
        reload_button,
    }
}

fn main() {
    const WIDTH: u32 = 800;
    const HEIGHT: u32 = 600;

    // Build the window.
    let mut events_loop = glium::glutin::EventsLoop::new();
    let window = glium::glutin::WindowBuilder::new()
        .with_title("Inserimento")
        .with_dimensions(WIDTH, HEIGHT);
    let context = glium::glutin::ContextBuilder::new()
        .with_vsync(true)
        .with_multisampling(4);
    let display = glium::Display::new(window, context, &events_loop).expect("cannot create display");

    // construct our `Ui`.
    let mut ui = conrod::UiBuilder::new([WIDTH as f64, HEIGHT as f64]).build();

    // Add a `Font` to the `Ui`'s `font::Map` from file.
    let assets = find_folder::Search::KidsThenParents(3, 5).for_folder("assets").expect("cannot find assets");
    let font_path = assets.join("fonts/NotoSans/NotoSans-Regular.ttf");
    ui.fonts.insert_from_file(font_path).expect("cannot load assets");

    // A type used for converting `conrod::render::Primitives` into `Command`s that can be used
    // for drawing to the glium `Surface`.
    let mut renderer = conrod::backend::glium::Renderer::new(&display).unwrap();

    // The image map describing each of our widget->image mappings (in our case, none).
    let image_map = conrod::image::Map::<glium::texture::Texture2d>::new();

    // Instantiate the generated list of widget identifiers.
    let ids = &mut Ids::new(ui.widget_id_generator());

    let mut value_text = String::new();
    let mut problem_text = String::new();
    let mut info = Info { teams: vec![], problems: 0, };
    let mut selected_team: Option<usize> = None;

    let mut config_file = File::open("config.toml").expect("cannot open config file");
    let mut contents = String::new();
    config_file.read_to_string(&mut contents).expect("cannot read config file");

    let config: BrokerConfig = toml::from_str(&contents).expect("cannot decode config file");

    let options = MqttOptions::new()
                    .set_keep_alive(5)
                    .set_reconnect(3)
                    .set_client_id(random::<u64>().to_string())
                    .set_broker(&config.address)
                    .set_user_name(&config.user)
                    .set_password(&config.password);

    let (tx, rx) = channel::<Info>();

    let tx = Arc::new(Mutex::new(tx));

    let callback = MqttCallback::new().on_message(move |message| {
        let tx = tx.lock().unwrap();
        match serde_json::from_slice(&*message.payload) {
            Ok(info) => tx.send(info).unwrap(),
            Err(_) => (),
        }
    });

    let mut client = MqttClient::start(options, Some(callback)).expect("cannot connect to mqtt broker");
    client.subscribe(vec![("info", QoS::Level2)]).expect("cannot subscribe");
    client.publish("ask_info", QoS::Level2, vec![69]).expect("cannot send ask_info request");

    // Poll events from the window.
    let mut event_loop = EventLoop::new();
    'main: loop {

        // Handle all events.
        for event in event_loop.next(&mut events_loop) {

            // Use the `winit` backend feature to convert the winit event to a conrod one.
            if let Some(event) = conrod::backend::winit::convert_event(event.clone(), &display) {
                ui.handle_event(event);
                event_loop.needs_update();
            }

            match event {
                glium::glutin::Event::WindowEvent { event, .. } => match event {
                    // Break from the loop upon `Escape`.
                    glium::glutin::WindowEvent::Closed |
                    glium::glutin::WindowEvent::KeyboardInput {
                        input: glium::glutin::KeyboardInput {
                            virtual_keycode: Some(glium::glutin::VirtualKeyCode::Escape),
                            ..
                        },
                        ..
                    } => break 'main,
                    _ => (),
                },
                _ => (),
            }
        }

        for i in rx.try_iter() {
            info = i;
        }

        // Instantiate all widgets in the GUI.
        set_widgets(ui.set_widgets(), ids, &mut value_text, &mut problem_text, &info, &mut selected_team, &mut client);

        // Render the `Ui` and then display it on the screen.
        if let Some(primitives) = ui.draw_if_changed() {
            renderer.fill(&display, primitives, &image_map);
            let mut target = display.draw();
            target.clear_color(0.0, 0.0, 0.0, 1.0);
            renderer.draw(&display, &mut target, &image_map).unwrap();
            target.finish().unwrap();
        }
    }
}

fn reset_values(value_text: &mut String, problem_text: &mut String, selected_team: &mut Option<usize>) {
    *value_text = String::new();
    *problem_text = String::new();
    *selected_team = None;
}

// Draw the Ui.
fn set_widgets(ref mut ui: conrod::UiCell, ids: &mut Ids, value_text: &mut String, problem_text: &mut String, info: &Info, selected_team: &mut Option<usize>, client: &mut MqttClient) {
    use conrod::{color, widget, Colorable, Labelable, Positionable, Sizeable, Widget};

    // Construct our main `Canvas` tree.
    widget::Canvas::new().flow_down(&[
        (ids.header, widget::Canvas::new().color(color::rgb(0.1, 0.1, 0.1)).border(0.0).pad_bottom(20.0)),
        (ids.body, widget::Canvas::new().length_weight(3.0).border(0.0).flow_down(&[
            (ids.team_problem_row, widget::Canvas::new().border(0.0).length_weight(2.0).flow_right(&[
                (ids.titled_team_box, widget::Canvas::new().color(color::WHITE).border(0.0).flow_down(&[
                    (ids.team_title_box, widget::Canvas::new().color(color::WHITE).border(0.0)),
                    (ids.team_box, widget::Canvas::new().color(color::WHITE).border(0.0)),
                ])),
                (ids.titled_problem_box, widget::Canvas::new().color(color::ORANGE).border(0.0).flow_down(&[
                    (ids.problem_title_box, widget::Canvas::new().color(color::WHITE).border(0.0)),
                    (ids.problem_box, widget::Canvas::new().color(color::WHITE).border(0.0)),
                ])),
            ])),
            (ids.value_box, widget::Canvas::new().color(color::WHITE).border(0.0)),
            (ids.send_box, widget::Canvas::new().color(color::WHITE).border(0.0).pad(20.0)),
        ])),
    ]).set(ids.master, ui);


    widget::Text::new("Squadra")
        .bottom_right_with_margins_on(ids.team_title_box, 10.0, 200.0)
        //.top_left_with_margins_on(ids.team_box, 10.0, 50.0)
        .left_justify()
        .font_size(24)
        .set(ids.team_title, ui);

    widget::Text::new("Problema")
        .bottom_left_with_margins_on(ids.problem_title_box, 10.0, 100.0)
        .left_justify()
        .font_size(24)
        .set(ids.problem_title, ui);

    widget::Text::new("Inserimento")
        .color(color::LIGHT_ORANGE)
        .font_size(48)
        .middle_of(ids.header)
        .set(ids.title, ui);

    let value = widget::TextBox::new(value_text)
        .middle_of(ids.value_box)
        .font_size(36)
        .center_justify()
        .set(ids.value, ui);

    for event in value {
        match event {
            widget::text_box::Event::Enter => (),
            widget::text_box::Event::Update(string) => {
                if string == "" {
                    *value_text = String::new();
                }
                if string == "J" || string == "j" {
                    *value_text = "J".to_string();
                }
                if let Ok(n) = string.parse::<u32>() {
                    if n < 10000 {
                        *value_text = string;
                    }
                }
            },
        }
    }

    let problem = widget::TextBox::new(problem_text)
        .top_left_with_margins_on(ids.problem_box, 10.0, 50.0)
        .font_size(36)
        .center_justify()
        .set(ids.problem, ui);

    for event in problem {
        match event {
            widget::text_box::Event::Enter => (),
            widget::text_box::Event::Update(string) => {
                if string == "" {
                    *problem_text = String::new();
                }
                if let Ok(n) = string.parse::<u32>() {
                    if n > 0 && n <= info.problems {
                        *problem_text = string;
                    }
                }
            },
        }
    }
    
    let team = widget::DropDownList::new(&info.teams, *selected_team)
        .top_right_with_margins_on(ids.team_box, 10.0, 50.0)
        .left_justify_label()
        .set(ids.team_selector, ui);

    for i in team {
        *selected_team = Some(i);
    }


    let send = widget::Button::new()
        .color(color::LIGHT_GREY)
        .label("Invia")
        .label_font_size(24)
        .center_justify_label()
        .middle_of(ids.send_box)
        .set(ids.send_button, ui);

    for _click in send {
        if info.teams.len() == 0 {
            client.publish("ask_info", QoS::Level2, vec![69]).unwrap();
        }
        if let Some(t) = *selected_team {
            if let Ok(p) = problem_text.parse::<u32>() {
                if let Ok(v) = value_text.parse::<u32>() {
                    let message = format!("{{ \"team\": {}, \"problem\": {}, \"value\": {} }}", t, p-1, v);
                    println!("{}", message);
                    client.publish("answers", QoS::Level2, message.into_bytes()).unwrap();

                    reset_values(value_text, problem_text, selected_team);
                }
                if *value_text == "J".to_string() {
                    let message = format!("{{ \"team\": {}, \"problem\": {}, \"value\": -1 }}", t, p-1);
                    println!("{}", message);
                    client.publish("answers", QoS::Level2, message.into_bytes()).unwrap();

                    reset_values(value_text, problem_text, selected_team);
                }
            }
        }
    }

}


pub struct EventLoop {
    ui_needs_update: bool,
    last_update: std::time::Instant,
}

impl EventLoop {
    pub fn new() -> Self {
        EventLoop {
            last_update: std::time::Instant::now(),
            ui_needs_update: true,
        }
    }

    /// Produce an iterator yielding all available events.
    pub fn next(
        &mut self,
        events_loop: &mut glium::glutin::EventsLoop,
    ) -> Vec<glium::glutin::Event> {

        // We don't want to loop any faster than 60 FPS, so wait until it has been at least 16ms
        // since the last yield.
        let last_update = self.last_update;
        let sixteen_ms = std::time::Duration::from_millis(16);
        let duration_since_last_update = std::time::Instant::now().duration_since(last_update);
        if duration_since_last_update < sixteen_ms {
            std::thread::sleep(sixteen_ms - duration_since_last_update);
        }

        // Collect all pending events.
        let mut events = Vec::new();
        events_loop.poll_events(|event| events.push(event));

        // If there are no events and the `Ui` does not need updating, wait for the next event.
        if events.is_empty() && !self.ui_needs_update {
            events_loop.run_forever(|event| {
                events.push(event);
                glium::glutin::ControlFlow::Break
            });
        }

        self.ui_needs_update = false;
        self.last_update = std::time::Instant::now();

        events
    }

    /// Notifies the event loop that the `Ui` requires another update whether or not there are any
    /// pending events.
    ///
    /// This is primarily used on the occasion that some part of the `Ui` is still animating and
    /// requires further updates to do so.
    pub fn needs_update(&mut self) {
        self.ui_needs_update = true;
    }
}


